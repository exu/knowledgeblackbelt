package exercises.e454;

import java.util.ArrayList;

public class Main {

	public static ArrayList<Customer> extractCustomers(ArrayList<BankAccount> accounts) {
		ArrayList<Customer> customers = new ArrayList<Customer>();

		for(BankAccount account : accounts) {
			if(!customers.contains(account.customer)) {
				customers.add(account.customer);
			}
		}

		return customers;
	}

	public static void main(String[] args) {

		Customer c1 = new Customer("Jack W.");
		Customer c2 = new Customer("Dan D.");

		BankAccount b1 = new BankAccount(c1);
		BankAccount b2 = new BankAccount(c2);
		BankAccount b3 = new BankAccount(c1);

		ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();

		accounts.add(b1);
		accounts.add(b2);
		accounts.add(b3);

		ArrayList<Customer> customers = Main.extractCustomers(accounts);

		System.out.println(customers);

	}
}

