package exercises.e43;

public class Product {
	private Supplier supplier;
	private String   name;
	
	public Product(String name, Supplier supplier) {
		this.supplier = supplier;
		this.name = name;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	
	
}
