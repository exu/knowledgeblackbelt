package exercises.e43;

public class Main {
	public static void main(String[] args) {
		Supplier s1 = new Supplier("IKEA");
		Supplier s2 = new Supplier("JavaBlackBelt");

		Product  p1 = new Product("Chair",   s1);
		Product  p2 = new Product("Table",   s1);
		Product  p3 = new Product("OO exam", s2);
		
		System.out.println(p1.getSupplier() == s1);
		
	}
}
