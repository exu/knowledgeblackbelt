package exercises.e352;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        BankAccount myAccount = new BankAccount();
        System.out.println(myAccount.getBalance()); // prints the default value 0.0

        myAccount.creditAccount(3.2);
        System.out.println(myAccount.getBalance()); // prints 3.2

        myAccount.debitAccount(1.2);
        System.out.println(myAccount.getBalance()); // prints 2.0

        System.out.println(myAccount.getTransactionCount()); // doesn't compile because addTransaction() is private!

	}

}
