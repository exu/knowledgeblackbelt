package exercises.e352;

public class BankAccount {
    // These fields are accessible only from within the BankAccount class
    private String accountNumber;
    public String getAccountNumber() {
		return accountNumber;
	}

	private double balance;
    private int transactionCount;

    public int getTransactionCount() {
		return transactionCount;
	}

	// These methods are accessible from anywhere in the code
    public void creditAccount(double amount) {
        balance += amount;
        addTransaction();
    }

    public void debitAccount(double amount) {
        balance -= amount;
        addTransaction();
    }

    public double getBalance() {
        return balance;
    }

    private void addTransaction() {
        transactionCount++;
    }
}