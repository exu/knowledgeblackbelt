package exercises.e312;

public class Customer {
	boolean goodCustomer;
	String name;
	Gender gender;
	
	public Customer(boolean goodCustomer, String name, Gender gender) {
		super();
		this.goodCustomer = goodCustomer;
		this.name = name;
		this.gender = gender;
	}

	
}
