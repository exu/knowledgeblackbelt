package exercises.e312;

/*

Create a customer class with 3 fields

good customer (boolean)
Name (String)
Gender (M/F)
Create a Main class with a static method that takes a Customer as argument.

If the customer is a good customer, say "Hello" and their name, otherwise, say "Go to hell" and their name.

If the customer is a woman and a good customer, add "What a nice dress!". Otherwise, simply say "Nice to see you again".

Note: for gender you may use a boolean, a String or a char, as you prefer. Java has the notion of enumerations too, but it is outside the scope of this course/exam.

Tip: You might skip ahead a little to the section on static method calls for some extra inspiration.
*/

public class App {
	public static void main(String[] args) {
		handleCustomer(new Customer(true,  "Jack Sparrow",   Gender.MALE));
		handleCustomer(new Customer(false, "Asia Lodziasia", Gender.FEMALE));
		handleCustomer(new Customer(true,  "Asia Lodziasia", Gender.FEMALE));
		handleCustomer(new Customer(false, "Marek Fifarek",  Gender.MALE));
	}
	
	public static void handleCustomer(Customer customer) {
		if(customer.goodCustomer) {
			System.out.print("Hello " + customer.name);
		} else {
			System.out.print("Go to hell");
		}
		
		if(customer.goodCustomer) {
			System.out.print(customer.gender == Gender.FEMALE ? " What a nice dress!" : " Nice to see you again");
		} 
		
		System.out.println();
	}
}
