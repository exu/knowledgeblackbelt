package exercises.e334;

/*

Create an Animal class with 3 fields:

String name
String type (eg. "Dog")
int age
In Animal, create a method birthday() that takes no parameter and adds one year to the age.

In a Main class with a main method:

Create an animal
Print its age
Call birthday()
Print its age again
Optional:

In the birthday method, change the animal's type to add "old" in front of it when the animal is more than 10 years old.

example: "Dog" -> "oldDog"


 */

public class Main {
	public static void main(String[] args) {
		Animal a = new Animal("Fuffy", "Doggy", 10);
		System.out.println(a.name + " " + a.age);
		a.birthday();
		System.out.println(a.name + " " + a.age);
	}
}
