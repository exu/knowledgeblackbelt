package exercises.e334;

public class Animal {
	String name;
	String type;
	int age;
	
	public Animal(String name, String type, int age) {
		this.name = name;
		this.type = type;
		this.age = age;
	}

	public void birthday() {
		this.age++;
		if(this.age > 10) {
			this.name = "old" + this.name;
		}
	}
}
