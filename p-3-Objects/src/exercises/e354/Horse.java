package exercises.e354;

public class Horse {
	private String name;
	private int maxHoursPerWeek;
	private int hoursActual;
	private boolean lazy;
	private int genLaz;
	
	private int geneticHours;
	
	
	public Horse(String name, int maxHoursPerWeek, int hoursActual,
			boolean lazy, int genLaz) {
		
		System.out.println("==================================");
		
		this.name = name;
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.hoursActual = hoursActual;
		this.lazy = lazy;
		this.genLaz = genLaz;
		
		if(validate()) {
			System.out.println(this);
		}
		
	}


	public Horse(String name, int maxHoursPerWeek, int hoursActual, boolean lazy) {
		System.out.println("==================================");
		this.name = name;
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.hoursActual = hoursActual;
		this.lazy = lazy;
		
		
		
		if(validate()) {
			System.out.println(this);
		}
	}
	
	
	
	
	@Override
	public String toString() {
		return name + " " + hoursActual + "/" + maxHoursPerWeek + " (" + lazy + "|" + genLaz + "|"+geneticHours+")";
	}


	private boolean validate() {
		geneticHours = (60 / (1 + genLaz));

		if(lazy && maxHoursPerWeek > geneticHours) {
			System.out.println(name + " - Can't work " + maxHoursPerWeek + " hours" + " only " + geneticHours);
			return false;
		}

		if(maxHoursPerWeek > 80) {
			System.out.println("Horse " + name +" cannot work more than 80h/week its not China");
			return false;
		}
		
		if(this.genLaz > 0 && !lazy) {
			System.out.println("Horse " + name + " is not lazy and cannot have lazy gen");
			return false;
		}
		
		return true;
	}


	public void addHour() {
		if(hoursActual < maxHoursPerWeek) {
			hoursActual++;
		} else {
			System.out.println("Horse " + name + " is tired");
		}
	}
	
	public void newWeek() {
		hoursActual = 0;
	}
	
}
