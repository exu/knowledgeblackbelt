package exercises.e351;

class BankAccount {
    String number;
    private double balance;
    
    public BankAccount(double balance, String number) {
		this.balance = balance;
		this.number = number;
	}
    
    void debit(double amount) {
        if (this.balance - amount > 0) {
           this.balance = this.balance - amount;
        } else { 
           throw new RuntimeException("Bug: some code attempts to remove too much money!");
        }
    }
    void addMoney(double amount) {
        this.balance += amount;
    }
    double getBalance() {
        return this.balance;
    }
  
}