package exercises.e351;

public class Main {
	public static void main(String[] args) {
        BankAccount johnBa = new BankAccount(100, "123-45676890-00");
        BankAccount nicolasBa = new BankAccount(500, "555-45676890-55");
        CrazyService.pay(6, johnBa, nicolasBa);
	}
}
