package exercises.e342;

/*

	Execute the following steps

	Write a method called johnConcat that receives a String as parameter and returns a String. The method simply concatenates the literal String "John" to the argument and returns the result.
	In a main method call your newly created method giving it an arbitrary String as argument, store the result in a local variable.
	Then test the result received against the expected result with both == and .equals()



 */

public class Main {
	public static void main(String[] args) {
		String value = "Rizzo";
		String result = johnConcat(value);
		System.out.println(result == "RizzoJohn");
		System.out.println(result.equals("RizzoJohn"));	
	}

	private static String johnConcat(String value) {
		return value + "John";
	}
}
