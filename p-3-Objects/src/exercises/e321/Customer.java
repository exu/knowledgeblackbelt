package exercises.e321;

public class Customer {
	boolean goodCustomer;
	String name;
	Gender gender;
	
	public Customer() {
		super();
		this.goodCustomer = true;
		this.name = "Jaś Fasola";
		this.gender = Gender.MALE;
	}
	
	
	public Customer(boolean goodCustomer, String name, Gender gender) {
		super();
		this.goodCustomer = goodCustomer;
		this.name = name;
		this.gender = gender;
	}

	
}
