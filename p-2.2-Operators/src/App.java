/**
 * @author exu
 *
 */
public class App {

	
	public static void ex223() {
		int v = 0;
		v++;
		int amount = v++;
		System.out.println(++v + " " + amount);
		System.out.println(v);
		
		// 3 1
		// 3
	}
	
	public static void ex227() {
		boolean canITakeHisMoney;
		int hisBalance = 5;
		long myBalance = 4;
		hisBalance += 8;
		canITakeHisMoney = hisBalance > myBalance;
		canITakeHisMoney = canITakeHisMoney && (hisBalance >= 3);
		System.out.println(canITakeHisMoney);
		
		// true
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ex223();
		ex227();

	}

}
