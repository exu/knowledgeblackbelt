/**
 * @author exu

 * Write a program that (in the given order):
 * 
 * - declares a long with 0 as initial value,
 * - prints its value to the console,
 * - changes the value to 3,
 * - prints the new value,
 * - declares a boolean with false as initial value,
 * - prints the value of the boolean.
 * 
 * Now for something trickier, write another program that:
 * 
 * - declares a double with no initial value,
 * - prints that variable value to the console.
 * 
 * Hint: the compiler should not be happy.
 *
 */
public class App {

	public static void ex01() {
		long lVar = 0;
		System.out.println("Long is: " + lVar);
		lVar = 3;
		System.out.println("Long is: " + lVar);
		
		boolean bVar = false;
		System.out.println("Bool is: " + bVar);
	}
	
	public static void ex02() {
		/*
		 * Unresolved compilation problem: 	The local variable dVar may not have been initialized
		 
		double dVar;
		
		try {
			System.out.println("Double is: " + dVar);
		} catch (Exception e) {
			System.out.println("We can't do this!" + e.getMessage());
		}
        */
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ex01();
		ex02();
	}
}
