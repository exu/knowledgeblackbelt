package javablackbelt;

/*

Use nested loops to display a multiplication matrix. A hardcoded local variable defines the size of the matrix. In the following examples, we display a 4x4 matrix. Your code should handle any value.

int mult = 4;
   
// your code goes here
The result will be

1 2 3 4
2 4 6 8
3 6 9 12  
4 8 12 16
*/

public class Ex4 {
	public static void main(String[] args) {
		int mult = 20;
		
		for(int i=1; i<=mult; i++) {
			int sum = 0;
			
			for(int j=1; j<=mult; j++) {
				System.out.printf("%4d", i*j); // oh look JAVA has printf :)
				sum += i*j;
			}
			
			System.out.println(" = " + sum);
		}
	}
}
