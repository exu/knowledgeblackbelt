package javablackbelt;

/**
 * Write a loop that displays numbers from 1 to 100 except multiples of 5. 50 is an exception: it should be printed.
 * 
 * Output:
 *
 * 1 2 3 4 6 7 8 9 11 12 13 14 16 17 18 19 21 ... 48 49 50 51 52 53 54 56 ... 89 91 92 93 94 96 97 98 99
 * Optional: insert a line break after 9, 19, 29,... use println() instead of print()
 *
 * @author exu
 *
 */
public class Ex3 {
	public static void main(String[] args) {
		for (int i = 1; i <= 100; i++) {
			if(i % 5 == 0 && i != 50) {
				continue;
			}
			
			if(i % 10 == 9) {
				System.out.println(i);
			} else {
				System.out.print(i + " ");
			}
		}
	}
}
