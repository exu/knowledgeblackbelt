package javablackbelt;

import java.lang.reflect.Array;

/**
 * Given 3 local variables defined as ints, 
 * write nested if statements to print them 
 * sorted in ascending order.
 *  
 * Even if we change the 3 variables' values, they should still 
 * be printed in ascending order.
 * 
 * @author exu
 *
 */
public class Ex1 {
	public static void main(String[] args) {
		run(12,55,1216); // 1 2 3
		run(22,12,316);  // 2 1 3
		run(12,34,6);    // 2 3 1
		run(1,45,6);     // 1 3 2
		run(100,45,6);   // 3 2 1
		run(100,45,56);  // 3 1 2
	}

	public static void run(int a, int b, int c) {
		int first;
		int second;
		int third;
		
		if(a < b && a < c) {
			first = a;
			second = b < c  ? b : c;
			third =  b >= c ? b : c;
		} else if (b < a &&  b < c) {
			first = b;
			second = a < c ? a : c;
			third = a >= c ? a : c;
		} else {
			first = c;
			second = b < a ? b : a;
			third = b >= a ? b : a;
		}
		
		System.out.println(first);
		System.out.println(second);
		System.out.println(third);
		System.out.println("================");
	}
}
