package javablackbelt;

/**
 * 
 * The FizzBuzz Challenge: Display numbers from 1 to x, 
 * replacing the word 'fizz' for multiples of 3, 
 * 'buzz' for multiples of 5 and 'fizzbuzz' for multiples of both 3 and 5.
 * 
 * @author exu
 *
 */
public class Ex2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int x = 100;
		String a;
		
		for (int i = 1; i <= x; i++) {
			a = "";
			
			if(i % 3 == 0) {
				a += "fizz";
			} 
			if (i % 5 == 0) {
				a += "buzz";
			} 
			
			if(i % 3 > 0 && i % 5 > 0){
				a += i;
			}

			a+=" ";
			
			System.out.printf(a);
		}
		
		
		

	}

}
