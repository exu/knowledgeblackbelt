/**
 * 
 */
package javablackbelt;

/**
 * @author exu
 *
 */

/*
Write a method called reverse, that takes an int as argument and returns an int. 
The method reverses the digits of the given number and returns it. If given 5433, 
the method returns 3345. Don't make it too difficult. Just small positive int's are ok :)

Call the method in a main with an arbitrary value and print the result to the screen.
 
 * */
public class Ex3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(reverse(123456789));
		System.out.println(reverse(987654321));
	}
	
	public static int reverse(int n) {
        int r = 0;

        while(n > 0){
                r = r * 10 + (n % 10);
                n /= 10;
        }
       
		return r;
	}
	
		
	

}
