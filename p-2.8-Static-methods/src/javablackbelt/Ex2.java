/**
 * 
 */
package javablackbelt;

/**
 * @author exu
 *
 */

/*

Write a method that validates a bank account number. 
The validation rule is simple: the last 2 digits are equal to 
the rest of the number modulo 97. Your method returns a boolean.

Example: 11114 is a valid number because 111 % 97 = 14.

In this example, the given number is 11114. 
We extract the last two digits (14) 
from the left part of the number (111). 
Then we verify if the first part (111) 
modulo 97 equals the second part (14).
 
 */
public class Ex2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int a = 11114;
		
		int firstPart = a / 100;
		int secondPart = a % 100;
		
		System.out.print((firstPart % 97 != secondPart ? "IN" : "") +  "VALID");
	}

}
