package javablackbelt;

/*
Write a method that receives a small integer in 
base 10 and prints out the original number represented in base 2.

For example, giving 8 will print out "1000".
*/

public class Ex4 {
	public static void main(String[] args) {
		System.out.print(Integer.toBinaryString(21000));
	}
}
