/**
 * 
 */
package javablackbelt;

/**
 * @author exu
 * Write a method that takes 2 int as arguments 
 * and returns their sum, call it from a main method 
 * and print the result.
 */
public class Ex1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.print(sum(2, 6));
	}
	
	public static int sum(int a, int b) {
		return a + b;
	}
	
}
